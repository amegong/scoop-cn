## Powershell
1. 打开`$PROFILE`文件。`vim $PROFILE`
2. 添加如下代码:
```
function set-proxy { $env:all_proxy="socks5://127.0.0.1:10808" }
function unset-proxy { $env:all_proxy="" }
```

## Unix
1. 打开脚本文件
2. 添加如下代码：
```
# 查看当前ip
alias ip="curl cip.cc"

alias set-proxy="export ALL_PROXY=socks5://127.0.0.1:10808

alias unset-proxy="unset ALL_PROXY"
```

或者打开代理软件时，终端自动代理。
以下代码添加到~/.zprofile文件中，该脚本会终端启动时运行
```
# 判断代理是否开启
# > /dev/null 的目的是避免将结果输出到屏幕上
lsof -i:10808 -sTCP:LISTEN > /dev/null

# $? 表示上一个命令的返回值，如果是0，则设置全局变量ALL_PROXY
if [ $? -eq 0 ]; then
    export ALL_PROXY=socks5://127.0.0.1:10808
    echo "[Tip] 成功连接代理！"
fi
```