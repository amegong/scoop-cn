## 安装

在PowerShell中运行

``` powershell
iwr -useb https://gitee.com/RubyKids/scoop-cn/raw/master/install.ps1 | iex
```

如果报错请先运行以下命令
``` powershell
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
```

## Help

上游正在开发新的安装方式，对于安装结果来说，同此仓库的旧安装方式暂时没有区别。 

若您发现通过此仓库方式安装失败，请您及时汇报情况，协助我们找到并解决问题，感谢。